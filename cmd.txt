C:\JavaTools\Java\jdk17\bin\jshell.exe monprojet.java


ObjVlisp obj = ObjVlispFabrique.nouveau();
var metaClass = obj.getClasse("Classe");
var systemClass = obj.getClasse("Systeme");

OObjet pointClass = (OObjet) metaClass.message(":nouveau", Map.of("nomClasse", "Point", "nomsAttributs", List.of("x","y")));
systemClass.message("afficher", pointClass);
pointClass.message(":message", "foo", (Message) (o, a) -> 1102);
pointClass.message("foo");

OObjet aPoint = (OObjet) pointClass.message(":nouveau", Map.of("x", 10, "y", 20));
OObjet anotherPoint = (OObjet) pointClass.message("nouveau");
anotherPoint.message(":x", 11);
anotherPoint.message(":y", 2);

aPoint.message("x");
aPoint.message("y");
anotherPoint.message("x");
anotherPoint.message("y");

((OObjetImpl) pointClass).getAttributs();
((OObjetImpl) aPoint).getAttributs();
((OObjetImpl) anotherPoint).getAttributs();





ObjVlisp obj = ObjVlispFabrique.nouveau();
OObjet metaClass = obj.getClasse("Classe");
OObjet systemClass = obj.getClasse("Systeme");

OObjet classA = (OObjet) metaClass.message(":nouveau", Map.of("nomClasse", "A"));
classA.message(":message", "foo", (Message) (o, a) -> 10);
classA.message(":message", "bar", (Message) (o, a) -> o.message("foo"));
OObjet classB = (OObjet) metaClass.message(":nouveau", Map.of("nomClasse", "B", "superClasse", classA));
classB.message(":message", "bar",
(Message) (o, a) -> (Integer) o.superMessage("bar") + (Integer) o.message("foo"));
OObjet classC = (OObjet) metaClass.message(":nouveau", Map.of("nomClasse", "C", "superClasse", classB));
classC.message(":message", "foo", (Message) (i, a) -> 50);

// classC.message(":message", "bar", (Message) (o, a) -> o.superMessage("bar"));

OObjet anA = (OObjet) classA.message("nouveau");
OObjet aB = (OObjet) classB.message("nouveau");
OObjet aC = (OObjet) classC.message("nouveau");

systemClass.message("afficher", (Object) anA.message("bar"));
systemClass.message("afficher", (Object) aB.message("bar"));
systemClass.message("afficher", (Object) aC.message("bar"));







ObjVlisp obj = ObjVlispFabrique.nouveau();
OObjet metaClass = obj.getClasse("Classe");
OObjet systemClass = obj.getClasse("Systeme");
OObjet booleanClass = obj.getClasse("Booleen");

OObjet trueObject = (OObjet) booleanClass.message("vrai");
OObjet falseObject = (OObjet) booleanClass.message("faux");

systemClass.message("afficher",trueObject.message("non"));
systemClass.message("afficher",falseObject.message("non"));

systemClass.message("afficher",falseObject.message("ou", falseObject, trueObject));
systemClass.message("afficher",trueObject.message("et", trueObject, falseObject));

