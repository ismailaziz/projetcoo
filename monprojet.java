import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

interface OObjet {
	Object message(String nom, Object... arguments);

	Object superMessage(String nom, Object... arguments);

	Object erreur(String cause);
}

interface ObjVlisp {
	OObjet getClasse(String nomClasse);
}

interface Message {
	Object apply(OObjet unObjet, Object... arguments);
}

@SuppressWarnings("unchecked")
class ObjVlispFabrique {

	private static Map<String, OObjet> mapClasses = new HashMap<>();

	private static void addAttrGetter(OObjet objet, String nomAttr) {
		Message getter = (o, a) -> ((OObjetImpl) o).getAttribut(nomAttr);
		((OObjetImpl) objet).setMessage(nomAttr, getter);
	}

	private static void addAttrSetter(OObjet objet, String nomAttr) {
		Message setter = (o, a) -> {
			Object valeur = a[0];
			((OObjetImpl) o).setAttribut(nomAttr, valeur);
			return o;
		};
		((OObjetImpl) objet).setMessage(":" + nomAttr, setter);
	}

	private static OObjetImpl newClass(OObjetImpl metaClasse, Map<String, Object> attributs) {
		OObjetImpl obj = new OObjetImpl(metaClasse);
		obj.setAttribut("nomsAttributs", new ArrayList<String>());
		obj.setAttribut("messages", new HashMap<String, Message>());
		// Ajout des attributs en arguments
		for (String nomAttribut : attributs.keySet()) {
			obj.setAttribut(nomAttribut, attributs.get(nomAttribut));
		}
		// Ajout des getters / setters selon le "nomsAttributs"
		OObjetImpl classe = obj;
		do {
			for (String nomAttribut : (List<String>) classe.getAttribut("nomsAttributs")) {
				addAttrGetter(obj, nomAttribut);
				addAttrSetter(obj, nomAttribut);
			}
			// Permet d'hériter des attributs de la chaîne d'héritage
			classe = (OObjetImpl) classe.getAttribut("superClasse");
		} while (classe != null);

		// Ajout des messages pour créer des instances
		obj.setMessage("nouveau", (Message) (o, a) -> newObject((OObjetImpl) o));
		obj.setMessage(":nouveau", (Message) (o, a) -> {
			Map<String, Object> attrs = (Map<String, Object>) a[0];
			return newObject((OObjetImpl) o, attrs);
		});

		return obj;
	}

	private static OObjetImpl newObject(OObjetImpl classe, Map<String, Object> attributs) {
		OObjetImpl obj = new OObjetImpl(classe);
		List<String> nomsAttributs = (List<String>) classe.getAttribut("nomsAttributs");
		for (String nomAttribut : attributs.keySet()) {
			// Permet de ne pas ajouter un attribut qui n'est pas dans la classe
			if (!nomsAttributs.contains(nomAttribut))
				continue;
			obj.setAttribut(nomAttribut, attributs.get(nomAttribut));
		}
		return obj;
	}

	private static OObjetImpl newObject(OObjetImpl classe) {
		return newObject(classe, Map.of());
	}

	private static void setDefault(OObjetImpl obj, String nomAttr, Object valAttr) {
		if (!obj.hasAttribut(nomAttr))
			obj.setAttribut(nomAttr, valAttr);
	}

	private static void addToClassMap(OObjetImpl c) {
		mapClasses.put((String) c.getAttribut("nomClasse"), c);
	}

	public static ObjVlisp nouveau() {

		OObjetImpl metaClass = new OObjetImpl();

		// Classe "Objet"
		OObjetImpl objectClass = new OObjetImpl(metaClass);
		objectClass.setAttribut("nomClasse", "Objet");
		objectClass.setAttribut("superClasse", null);
		objectClass.setAttribut("nomsAttributs", List.of("classe"));
		objectClass.setAttribut("messages", new HashMap<String, Message>());
		objectClass.setMessage("nouveau", (Message) (o, a) -> newObject((OObjetImpl) o));
		objectClass.setMessage("classe", (Message) (o, a) -> ((OObjetImpl) o).getAttribut("classe"));
		objectClass.setMessage("toString", (Message) (o, a) -> ((OObjetImpl) o).toString());

		// Classe "Classe"
		metaClass.setAttribut("nomClasse", "Classe");
		metaClass.setAttribut("classe", metaClass);
		metaClass.setAttribut("superClasse", objectClass);
		metaClass.setAttribut("nomAttributs", List.of("nomClasse", "nomAttributs", "messages", "superClasse"));
		metaClass.setAttribut("messages", new HashMap<String, Message>());
		metaClass.setMessage(":nouveau", (Message) (o, a) -> {
			Map<String, Object> attributs = (Map<String, Object>) a[0];
			OObjetImpl nouvelleClasse = newClass((OObjetImpl) o, attributs);
			setDefault(nouvelleClasse, "superClasse", objectClass);
			return nouvelleClasse;
		});
		metaClass.setMessage(":message", (Message) (o, a) -> {
			String nomMessage = (String) a[0];
			Message message = (Message) a[1];
			((OObjetImpl) o).setMessage(nomMessage, message);
			return o;
		});
		metaClass.setMessage(":accept", (Message) (o, a) -> {
			for (Object obj : a) {
				String nomMessage = (String) obj;
				metaClass.setMessage(nomMessage, (Message) (o2, a2) -> o.erreur("Message non-implémenté"));
			}
			return o;
		});

		// Classe "Systeme"
		OObjetImpl systemClass = (OObjetImpl) metaClass.message(":nouveau", Map.of("nomClasse", "Systeme"));
		systemClass.setMessage("afficher", (Message) (o, a) -> {
			Object obj = a[0];
			String str;
			if (obj instanceof OObjet)
				str = ((OObjet) obj).message("toString").toString();
			else
				str = obj.toString();
			System.out.println(str);
			return o;
		});

		// Classe "Booleen"
		OObjetImpl booleanClass = (OObjetImpl) metaClass.message(":nouveau", Map.of("nomClasse", "Booleen"));

		OObjetImpl trueClass = (OObjetImpl) metaClass.message(":nouveau",
				Map.of("nomClasse", "Vrai", "superClasse", booleanClass));
		OObjetImpl trueObject = (OObjetImpl) trueClass.message("nouveau");
		trueClass.setMessage("nouveau", (Message) (o, a) -> trueObject); // Pour avoir une sorte de singleton
		trueClass.setMessage("toString", (Message) (o, a) -> "Vrai");

		OObjetImpl falseClass = (OObjetImpl) metaClass.message(":nouveau",
				Map.of("nomClasse", "Faux", "superClasse", booleanClass));
		OObjetImpl falseObject = (OObjetImpl) falseClass.message("nouveau");
		falseClass.setMessage("nouveau", (Message) (o, a) -> falseObject);
		falseClass.setMessage("toString", (Message) (o, a) -> "Faux");

		trueClass.setMessage("non", (Message) (o, a) -> falseObject);
		falseClass.setMessage("non", (Message) (o, a) -> trueObject);

		booleanClass.setMessage("vrai", (Message) (o, a) -> trueObject);
		booleanClass.setMessage("faux", (Message) (o, a) -> falseObject);

		booleanClass.setMessage("ou", (Message) (o, a) -> {
			if (((OObjetImpl) o).getAttribut("classe") == trueClass)
				return trueObject;
			for (Object obj : a) {
				if (((OObjetImpl) obj).getAttribut("classe") == trueClass)
					return trueObject;
			}
			return falseObject;
		});

		booleanClass.setMessage("et", (Message) (o, a) -> {
			if (((OObjetImpl) o).getAttribut("classe") == falseClass)
				return falseObject;
			for (Object obj : a) {
				if (((OObjetImpl) obj).getAttribut("classe") == falseClass)
					return falseObject;
			}
			return trueObject;
		});

		// Ajout des classes
		addToClassMap(metaClass);
		addToClassMap(objectClass);
		addToClassMap(systemClass);
		addToClassMap(booleanClass);

		// Création de ObjVlisp
		ObjVlisp objVlisp = new ObjVlispImpl(mapClasses);
		return objVlisp;
	}
}

class ObjVlispImpl implements ObjVlisp {

	private Map<String, OObjet> mapClasses;

	public ObjVlispImpl(Map<String, OObjet> mapClasses) {
		this.mapClasses = mapClasses;

	}

	public OObjet getClasse(String nomClasse) {
		return mapClasses.get(nomClasse);
	}
}

class OObjetImpl implements OObjet {
	private final Map<String, Object> attributs = new HashMap<>();

	public OObjetImpl(OObjet classe, Map<String, Object> attributs) {
		this.attributs.put("classe", classe);
		for (Entry<String, Object> attr : attributs.entrySet()) {
			this.attributs.put(attr.getKey(), attr.getValue());
		}
	}

	public OObjetImpl(OObjet classe) {
		this(classe, Map.of());
	}

	public OObjetImpl() {
		this(null);
	}

	@Override
	public Object message(String nomMessage, Object... arguments) {
		Message m = messageLookUp(nomMessage);
		if (m == null)
			return erreur("Message inexistant");
		return m.apply(this, arguments);
	}

	@Override
	public Object superMessage(String nomMessage, Object... arguments) {
		Message m = superMessageLookUp(nomMessage);
		if (m == null)
			return erreur("Message inexistant");
		return m.apply(this, arguments);
	}

	@Override
	public Object erreur(String cause) {
		throw new RuntimeException(cause);
	}

	private OObjetImpl getDefiningClass(String nomMessage) {
		if (hasMessage(nomMessage))
			return this;
		OObjetImpl superClasse = getSuperClasse();
		if (superClasse == null)
			return null;
		return superClasse.getDefiningClass(nomMessage);
	}

	private Message messageLookUp(String nomMessage) {
		if (hasMessage(nomMessage))
			return getMessage(nomMessage);
		OObjetImpl definingClass = getClasse().getDefiningClass(nomMessage);
		if (definingClass == null)
			return null;
		return definingClass.getMessage(nomMessage);
	}

	private Message superMessageLookUp(String nomMessage) {
		OObjetImpl classe = getClasse();
		OObjetImpl definingClass = classe.getDefiningClass(nomMessage);
		if (true)
			definingClass = definingClass.getSuperClasse();
		if (definingClass == null)
			return null;
		return definingClass.getMessage(nomMessage);
	}

	@Override
	public String toString() {
		try {
			return "classe " + getAttribut("nomClasse").toString();
		} catch (Exception e1) {
			try {
				return "instance de la " + getAttribut("classe").toString();
			} catch (Exception e2) {
				return "error";
			}
		}
	}

	public Map<String, Object> getAttributs() {
		return attributs;
	}

	public Object getAttribut(String nom) {
		return attributs.get(nom);
	}

	public void setAttribut(String nom, Object valeur) {
		attributs.put(nom, valeur);
	}

	public boolean hasAttribut(String nom) {
		return attributs.containsKey(nom);
	}

	public void setMessage(String nomMessage, Message message) {
		getMessages().put(nomMessage, message);
	}

	@SuppressWarnings("unchecked")
	private Map<String, Message> getMessages() {
		return (Map<String, Message>) attributs.get("messages");
	}

	private boolean hasMessage(String nomMessage) {
		if (!attributs.containsKey("messages"))
			return false;
		return getMessages().containsKey(nomMessage);
	}

	private Message getMessage(String nomMessage) {
		return getMessages().get(nomMessage);
	}

	private OObjetImpl getClasse() {
		return (OObjetImpl) attributs.get("classe");
	}

	private OObjetImpl getSuperClasse() {
		return (OObjetImpl) attributs.get("superClasse");
	}
}